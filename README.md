## Changes that need to be made before project is viable
You can find and replace these values
- Development Team ID [$REPLACE_DEVELOPMENT_TEAM_ID]
- Distribution Certificate [$REPLACE_DISTRIBUTION_CERTIFICATE]
- Apple ID [$REPLACE_APPLE_ID]

Please also setup the project with App Store Connect and create certificates using `match` for this project.
Refer to the fastlane documentation for further information
